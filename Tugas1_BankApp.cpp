#include <stdio.h>
#include <cstdlib>
#include <string.h>

char namaFileBank[]="banks.dat";
char banks[1000][1000];
int exitMenu=0;
//detect file
bool DeteksiFile(char namafile []){
	FILE *f;
	f=fopen(namafile,"r");
	if(f!=NULL){
		return true;
	}
	else{
		return false;
	}
}

void AmbilDataBank(char namafile []){
	FILE *f;
	f=fopen(namafile,"r");
	int i=0;
	while(!feof(f)){
		fscanf(f,"%s\n",&banks[i]);	
		i++;
	}
	fclose(f);
}

void TampilDaftarBank(){
	printf("====Daftar Bank=====\n");
	bool selesai=false;
	int i=0;
	char namaBank[1000]="";
	do{
		strcpy(namaBank,banks[i]);
		
		if(strlen(namaBank)==0){
			selesai=true;
			printf("%d. %s \n",(i+1),"Exit");
			exitMenu=i+1;
		}
		else{
			printf("%d. %s \n",(i+1),namaBank);
		}
		i++;
	}while(!selesai);
	printf("====================\n");
}
void TampilBankMenu(int indexBank){
	printf("====Menu Bank %s\n",banks[indexBank]);
	printf("1. Tambah Nasabah\n");
	printf("2. Hapus Nasabah\n");
	printf("3. Ubah Nasabah\n");
	printf("4. Cetak Nasabah\n");
	printf("5. Kembali Ke Daftar Bank\n");
	printf("====================\n");
}

void BankMenu(int indexBank){
	bool selesai=false;
	do{
		system("cls");
		int pilih=0;
		TampilBankMenu(indexBank);
		printf("Pilih : ");
		scanf("%d",&pilih);
		switch(pilih){
			case 1:
				
				break;
			case 2:
				
				break;
			case 3:
				
				break;
			case 4:
				
				break;
			case 5:
				selesai=true;
				break;
		}
		
	}while(!selesai);
}

int main(){
	AmbilDataBank(namaFileBank);
	bool selesai=false;
	do{
		int pilih=0;
		TampilDaftarBank();
		printf("Pilih : ");
		scanf("%d",&pilih);
		if(pilih==exitMenu){
			selesai=true;
		}		
		else{
			if(pilih>0&&pilih<exitMenu-1){
				BankMenu(pilih-1);
			}
		}
		system("cls");
	}while(!selesai);
}
